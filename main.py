import constants
import function.detector as detector
import function.picam as picam
import function.emailfx as email
from function.remote_control import TelegramRemoteControl
from function.config_cache import ConfigCache
import utils.commonutils as commonutils

from cv2 import imwrite
from googlecloud.googledrive import GoogleDrive
from concurrent.futures import ThreadPoolExecutor
from configparser import ConfigParser

import os
import argparse
import logging.config
import threading
import time


def init_config_cache() -> ConfigCache:
    """
    Initialize a cache which stores changeable data/flag to control the program
    """
    config = ConfigCache()
    config.update(constants.CONFIG_CAMERA_DETECTION_ENABLED, CAMERA_ENABLE_DETECTION)
    config.update(constants.CONFIG_CAMERA_CAPTURE_ENABLED, False)
    config.update(constants.CONFIG_BIND_USER_ID, TELEGRAM_BIND_USER_ID)
    return config


def upload_record(google_drive: GoogleDrive, base_folder: str, file_path: str):
    """
    upload the recordings to google drive
    """
    logger.info("Start uploading...")
    try:
        files = google_drive.search_file(filename=base_folder, mime_type="application/vnd.google-apps.folder")

        base_folder_id = None
        if len(files) < 1:
            base_folder_id = google_drive.create_folder(base_folder)
        else:
            base_folder_id = files[0]["id"]

        google_drive.upload_file(parents=[base_folder_id], file_path=file_path)
        logger.info(f"Completed uploading the file from {file_path}")
    except Exception as e:
        logger.error("Failed to upload recordings to google drive", e)


def send_email(sender_address: str, receiver_address: str, password: str, subject: str, body: str, filename: str,
               file_path: str):
    """
    send email notification
    """
    subject = commonutils.replace_datetime_with_current_time(subject, constants.DATETIME_FORMAT_2)
    body = commonutils.replace_datetime_with_current_time(body, constants.DATETIME_FORMAT_2)
    email.send_email(sender_address, receiver_address, password, subject, body, filename, file_path)


def run_camera_capture():
    """
    capture an image using camera and send email with the captured image to a preset email address
    """
    # create a folder to contain the captured image
    output_dir_new = commonutils.make_folder_with_datetime(CAMERA_OUTPUT_DIR, constants.DATETIME_FORMAT_1, 0o744)
    img_file_path = output_dir_new + os.sep + CAMERA_IMG_FILENAME
    picam.capture_image(img_file_path, CAMERA_FRAME_WIDTH, CAMERA_FRAME_HEIGHT)

    logger.info("Sending email...")
    send_email(EMAIL_SENDER_ADDR, EMAIL_RECEIVER_ADDR, EMAIL_PASSWORD, EMAIL_CAPTURE_MODE_SUBJECT,
               EMAIL_CAPTURE_MODE_BODY, CAMERA_IMG_FILENAME, img_file_path)


def run_camera_detection(is_on: callable, google_drive: GoogleDrive):
    """
    main logic of detection mode. When it detects a person, it will send an email with a captured image to
    preset email address as email notification. Then it starts to record videos. If google drive backup is enabled,
    it will upload the recordings to cloud.
    """

    # prepare google cloud
    executor = None
    if GDRIVE_ENABLE_CLOUD_BACKUP:
        executor = ThreadPoolExecutor(max_workers=GDRIVE_NUM_OF_THREAD)

    # forever loop to do detection logic
    while True and is_on():
        try:
            logger.info("Start to run detector...")
            result_list = detector.run(CAMERA_MODEL, CAMERA_ID, CAMERA_FRAME_WIDTH, CAMERA_FRAME_HEIGHT,
                                       CAMERA_NUM_OF_THREAD, CAMERA_ENABLE_EDGE_TPU, CAMERA_DETECT_ALLOW_LIST, is_on)
            # print(resultList)
            if is_on():
                # create a folder to store the image and video for the detected case
                output_dir_new = commonutils.make_folder_with_datetime(
                    CAMERA_OUTPUT_DIR, constants.DATETIME_FORMAT_1, 0o744)

                # save the captured image
                logger.info("Saving the captured image...")
                img_file_path = output_dir_new + CAMERA_IMG_FILENAME
                imwrite(img_file_path, result_list["image"])

                logger.info("Sending email...")
                try:
                    send_email(EMAIL_SENDER_ADDR, EMAIL_RECEIVER_ADDR, EMAIL_PASSWORD, EMAIL_SUBJECT, EMAIL_BODY,
                               CAMERA_IMG_FILENAME, img_file_path)
                except Exception as e:
                    logger.error("Failed to send email.", e)

                logger.info("Recording...")
                for i in range(CAMERA_NUM_OF_RECORDING):
                    file_path = picam.record(output_dir_new, CAMERA_FRAME_WIDTH, CAMERA_FRAME_HEIGHT,
                                             CAMERA_RECORDING_DURATION_IN_SEC,
                                             CAMERA_RECORDING_DURATION_IN_SEC)
                    logger.info(f"file: {file_path}")

                    if GDRIVE_ENABLE_CLOUD_BACKUP:
                        logger.info("Uploading...")
                        t = executor.submit(upload_record, google_drive, GDRIVE_BASE_FOLDER, file_path)
                logger.info("Recording completed.")

            if not is_on():
                logger.info("run_camera_detection is switched off.")
        except Exception as e:
            logger.error(e)


def main():
    """
    Initialize TG remote control.
    Control the activation of the detection mode and the capture mode
    """
    #  init config cache
    config_cache = init_config_cache()

    # write the pid to an external file for operation.
    pid = os.getpid()
    with open("pid.txt", "w") as f:
        f.write(str(pid))

    # If the user enable Google Drive back up, initialize it first for ensuring the authentication is done properly.
    google_drive = None
    if GDRIVE_ENABLE_CLOUD_BACKUP:
        try:
            google_drive = GoogleDrive(GDRIVE_CLIENT_SECRET_FILE, GDRIVE_TOKEN_FILE, GDRIVE_API_NAME
                                       , GDRIVE_API_VERSION, GDRIVE_SCOPES)
        except Exception as e:
            logger.error("Failed to initialize google drive service.", e)
            # no token file means it is the first time to do Auth 2.0
            # For other cases such as network issue, the application is allowed to run.
            if not os.path.exists(GDRIVE_TOKEN_FILE):
                logger.info("Authentication must be done before using this application.")
                exit()

    if TELEGRAM_ENABLE_REMOTE_CONTROL:
        # create a thread to poll the telegram server and update the config in config cache after receiving instructions
        logger.info(f"Telegram bot is starting up...")
        # Telebot retries the network connection if the connection fails
        tg_bot = TelegramRemoteControl(TELEGRAM_AUTH_TOKEN, config_cache)
        tg_thread = threading.Thread(target=tg_bot.run)
        tg_thread.start()

    # run detection flow
    is_on = config_cache.get(constants.CONFIG_CAMERA_DETECTION_ENABLED)
    is_last_on = False

    logger.info(f"Detection is {is_on}")

    # forever loop to check the is_on flag and is_capture_on flag in config cache
    # Only activate either detection mode or capture mode at a time.
    detector_thread = None
    while True:
        logger.info(f"Detection mode: {is_on}.")

        # Activate detection mode
        if is_on and not is_last_on:
            logger.info(f"Detection is starting up...")
            detector_thread = threading.Thread(target=run_camera_detection, args=(lambda: is_on, google_drive))
            detector_thread.start()

        is_last_on = is_on
        time.sleep(3)  # slow down the process

        is_capture_on = config_cache.get(constants.CONFIG_CAMERA_CAPTURE_ENABLED)

        # Activate capture mode
        if is_capture_on:
            logger.info(f"Capture mode is starting up...")
            is_detection_on = config_cache.get(constants.CONFIG_CAMERA_DETECTION_ENABLED)

            # temporarily disable camera mode if it is originally enabled
            if is_detection_on:
                is_on = False
                config_cache.update(constants.CONFIG_CAMERA_DETECTION_ENABLED, False)

            if detector_thread:
                logger.info(f"Pending the detection mode from stopping...")
                detector_thread.join()  # wait for the thread of camera mode to exit
            logger.info(f"Capture mode is on...")
            capture_thread = threading.Thread(target=run_camera_capture)
            capture_thread.start()
            capture_thread.join()

            # set the capture mode to false after completing the capture
            config_cache.update(constants.CONFIG_CAMERA_CAPTURE_ENABLED, False)

            # reset the flag in the config cache if it is originally enabled.
            if is_detection_on:
                config_cache.update(constants.CONFIG_CAMERA_DETECTION_ENABLED, True)

            is_last_on = False
            logger.info(f"Capture mode is completed...")

        # renew the flag of camera mode
        is_on = config_cache.get(constants.CONFIG_CAMERA_DETECTION_ENABLED)
        if is_last_on != is_on:
            logger.info(f"Detection is switched from {is_last_on} to {is_on}")


if __name__ == '__main__':
    # load logger setting
    commonutils.load_logging('./config/logging.yaml')
    logger = logging.getLogger(__name__)

    # argument parser
    arg_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    arg_parser.add_argument(
        '--config', help='the location of configuration file', required=True)
    args = arg_parser.parse_args()

    parser = ConfigParser()
    parser.read(args.config)

    # camera and tensorflow setting
    CAMERA_ENABLE_DETECTION = parser["CAMERA"]["camera_enable_detection"]
    CAMERA_MODEL = parser["CAMERA"]["camera_model"]
    CAMERA_ID = int(parser["CAMERA"]["camera_id"])
    CAMERA_FRAME_WIDTH = int(parser["CAMERA"]["camera_frame_width"])
    CAMERA_FRAME_HEIGHT = int(parser["CAMERA"]["camera_frame_height"])
    CAMERA_NUM_OF_THREAD = int(parser["CAMERA"]["camera_num_of_thread"])
    CAMERA_ENABLE_EDGE_TPU = "True" == parser["CAMERA"]["camera_enable_edge_tpu"]
    CAMERA_DETECT_ALLOW_LIST = parser["CAMERA"]["camera_detect_allow_list"].split(",")
    CAMERA_OUTPUT_DIR = parser["CAMERA"]["camera_output_dir"]
    CAMERA_IMG_FILENAME = parser["CAMERA"]["camera_img_filename"]
    CAMERA_RECORDING_DURATION_IN_SEC = int(parser["CAMERA"]["camera_recording_duration_in_sec"])
    CAMERA_NUM_OF_RECORDING = int(parser["CAMERA"]["camera_num_of_recording"])

    # send email setting
    EMAIL_SENDER_ADDR = parser["EMAIL"]["email_sender_addr"]
    EMAIL_RECEIVER_ADDR = parser["EMAIL"]["email_receiver_addr"]
    EMAIL_PASSWORD = parser["EMAIL"]["email_password"]
    EMAIL_SUBJECT = parser["EMAIL"]["email_subject"]
    EMAIL_BODY = parser["EMAIL"]["email_body"]
    EMAIL_CAPTURE_MODE_SUBJECT = parser["EMAIL"]["email_capture_mode_subject"]
    EMAIL_CAPTURE_MODE_BODY = parser["EMAIL"]["email_capture_mode_body"]

    # Google Drive setting
    GDRIVE_ENABLE_CLOUD_BACKUP = "True" == parser["GOOGLE_DRIVE"]["gdrive_enable_cloud_backup"]
    GDRIVE_CLIENT_SECRET_FILE = parser["GOOGLE_DRIVE"]["gdrive_client_secret_file"]
    GDRIVE_TOKEN_FILE = parser["GOOGLE_DRIVE"]["gdrive_token_file"]
    GDRIVE_API_NAME = parser["GOOGLE_DRIVE"]["gdrive_api_name"]
    GDRIVE_API_VERSION = parser["GOOGLE_DRIVE"]["gdrive_api_version"]
    GDRIVE_SCOPES = parser["GOOGLE_DRIVE"]["gdrive_scopes"].split(",")
    GDRIVE_BASE_FOLDER = parser["GOOGLE_DRIVE"]["gdrive_base_folder"]
    no_of_thread = parser["GOOGLE_DRIVE"]["gdrive_num_of_thread"]
    GDRIVE_NUM_OF_THREAD = int(no_of_thread) if no_of_thread else None

    # Telegram remote control
    TELEGRAM_ENABLE_REMOTE_CONTROL = "True" == parser["TELEGRAM_REMOTE_CONTROL"][
        "telegram_enable_remote_control"]
    TELEGRAM_AUTH_TOKEN = parser["TELEGRAM_REMOTE_CONTROL"]["telegram_auth_token"]
    TELEGRAM_BIND_USER_ID = parser["TELEGRAM_REMOTE_CONTROL"]["telegram_bind_user_id"]

    main()
