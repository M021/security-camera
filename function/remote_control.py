import telebot
import constants
from function.config_cache import ConfigCache
import logging.config
import os

logger = logging.getLogger(__name__)

MSG_INCORRECT = 'Incorrect'
MSG_INCORRECT_PARAM = 'Incorrect param'
MSG_DONE = 'Done'
MSG_FAILED = 'Failed'
MSG_NO_CHANGE = 'No change'

"""
It is not a recommended method to implement as a remote control.
By receiving message as an instruction via Telegram, the instruction will update the config cache
which is used to control the application.
"""


class TelegramRemoteControl():

    def __init__(self, auth_token: str, config_cache: ConfigCache):
        self.auth_token = auth_token
        self.config_cache = config_cache
        self.bot = telebot.TeleBot(self.auth_token)
        self.bot.register_message_handler(callback=self.cam_status_check, commands=['status'])
        self.bot.register_message_handler(callback=self.cam_switch, commands=['switchOn', 'switchOff'])
        self.bot.register_message_handler(callback=self.cam_capture, commands=['capture'])
        self.bot.register_message_handler(callback=self.shutdown, commands=['shutdown'])

    def run(self):
        self.bot.infinity_polling()  # polling for new message from users

    def __check_user_auth(self, user_id):
        """
        Simple authentication by comparing the user id from the configuration value and the received value from telegram
        """
        logger.debug(f"Check user id {user_id}")

        try:
            result = int(self.config_cache.get(constants.CONFIG_BIND_USER_ID))

            if user_id and user_id == result:
                logger.debug(f"Authentication successful - user id {user_id}")
                return True

            logger.debug(f"Authentication failed - input user id {user_id}, actual user id: {result}")
        except Exception as e:
            logger.error("Failed to user authentication.", e)

        return False

    def cam_capture(self, message):
        if not self.__check_user_auth(message.chat.id):
            self.bot.send_message(message.chat.id, MSG_INCORRECT)
            return

        self.config_cache.update(constants.CONFIG_CAMERA_CAPTURE_ENABLED, True)
        self.bot.send_message(message.chat.id, MSG_DONE)

    def cam_status_check(self, message):
        if not self.__check_user_auth(message.chat.id):
            self.bot.send_message(message.chat.id, MSG_INCORRECT)
            return

        msg = None
        try:
            result = self.config_cache.get(constants.CONFIG_CAMERA_DETECTION_ENABLED)
            if result:
                status = constants.CONFIG_ON
            else:
                status = constants.CONFIG_OFF
            msg = f"status: {status}"
        except Exception as e:
            logger.error("Failed to check camera status.", e)
            msg = MSG_FAILED
        self.bot.send_message(message.chat.id, msg)

    def cam_switch(self, message):
        if not self.__check_user_auth(message.chat.id):
            self.bot.send_message(message.chat.id, MSG_INCORRECT)
            return

        request = message.text.split()

        param = request[0]
        instruction = constants.CONFIG_ON if param == "/switchOn" else constants.CONFIG_OFF

        try:
            result = self.config_cache.get(constants.CONFIG_CAMERA_DETECTION_ENABLED)
            if result:
                status = constants.CONFIG_ON
            else:
                status = constants.CONFIG_OFF

            if instruction == status:
                self.bot.send_message(message.chat.id, MSG_NO_CHANGE)
                return
            else:
                logger.debug(f"update {constants.CONFIG_CAMERA_DETECTION_ENABLED} to {instruction}")
                self.config_cache.update(constants.CONFIG_CAMERA_DETECTION_ENABLED, instruction == constants.CONFIG_ON)
                msg = MSG_DONE
        except Exception as e:
            logger.error("Failed to switch on/off the camera.", e)
            msg = MSG_FAILED

        self.bot.send_message(message.chat.id, msg)

    def shutdown(self, message):
        logger.info("Shutdown Raspberry Pi...")
        os.system('sudo shutdown')
        self.bot.send_message(message.chat.id, MSG_DONE)

    def shut_down(self):
        self.bot.stop_bot()
        self.bot.close()
