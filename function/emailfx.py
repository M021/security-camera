import smtplib, ssl, logging

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_email(sender_address: str, receiver_address: str, password: str, subject: str, body: str, filename: str,
               filepath: str):
    # email content
    message = MIMEMultipart()
    message["From"] = sender_address
    message["To"] = receiver_address
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))

    # load attachment
    part = MIMEBase("application", "octet-stream")

    with open(filepath, "rb") as attachment:
        part.set_payload(attachment.read())

    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {filename}",
    )

    message.attach(part)

    text = message.as_string()

    # send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_address, password)
        server.sendmail(sender_address, receiver_address, text)
        logging.info('The email is sent successfully.')
