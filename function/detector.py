import sys
import cv2
import logging
import json

from tflite_support.task import core
from tflite_support.task import processor
from tflite_support.task import vision
from google.protobuf.json_format import MessageToJson

logger = logging.getLogger(__name__)

"""
This function is to carry out core the logic of detection with the help of tensorFlow lite.
"""

DETECTION_THRESHOLD = 0.4

def run(model: str, camera_id: int, frame_width: int, frame_height: int, num_threads: int,
        enable_edgetpu: bool, detect_allow_list: list, is_on) -> dict:
    cap = cv2.VideoCapture(camera_id)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)

    base_options = core.BaseOptions(
        file_name=model, use_coral=enable_edgetpu, num_threads=num_threads)
    detection_options = processor.DetectionOptions(
        max_results=3, score_threshold=0.4, class_name_allowlist=detect_allow_list)
    options = vision.ObjectDetectorOptions(
        base_options=base_options, detection_options=detection_options)
    detector = vision.ObjectDetector.create_from_options(options)

    logger.info(f"detector is on? {is_on()}. video capture is on? {cap.isOpened()}")
    try:
        while cap.isOpened() and is_on():
            success, image = cap.read()
            if not success:
                logger.error("Failed to read from pi camera. Please verify your pi camera settings.")
                sys.exit()

            rgb_image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)

            input_tensor = vision.TensorImage.create_from_array(rgb_image)

            detection_result = detector.detect(input_tensor)

            # You can get the score from detection_result and compare it with the threshold you set.
            detected = False
            if len(detection_result.detections) >= 1:
                json_text = MessageToJson(detection_result)
                jsonObj = json.loads(json_text)
                for item in jsonObj['detections']:
                    score = item['classes'][0]['score']
                    logger.info(f"score: {score}")
                    if score > DETECTION_THRESHOLD:
                        detected = True

            if detected:
                result_dic = {"detection_result": detection_result.detections, "image": image}
                return result_dic
    finally:
        logger.info("Detector is going to exit.")
        cap.release()
        cv2.destroyAllWindows()
