from picamera import PiCamera
from datetime import datetime
from time import sleep

DATETIME_FORMAT = '%Y%m%d_%H%M%S'

"""
Using picamera library to implement some useful functions.
"""


def capture_image(output_file_path: str, width: int, height: int):
    with PiCamera() as camera:
        camera.resolution = (width, height)
        sleep(3)
        camera.capture(output_file_path)


def record(output_dir: str, frame_width: int, frame_height: int, duration_in_sec: int, split_in_sec: int):
    num_of_split = duration_in_sec / split_in_sec
    file_ext = '.h264'
    with PiCamera() as camera:
        try:
            cnt = 1
            camera.resolution = (frame_width, frame_height)
            sleep(3)

            file_path = output_dir + datetime.now().strftime(DATETIME_FORMAT) + file_ext
            camera.start_recording(file_path)
            camera.wait_recording(split_in_sec)
            while cnt < num_of_split:
                camera.split_recording(output_dir + datetime.now().strftime(DATETIME_FORMAT) + file_ext)
                camera.wait_recording(split_in_sec)
                cnt = cnt + 1
        finally:
            camera.stop_recording()
    return file_path
