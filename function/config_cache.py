from threading import Lock
import logging

logger = logging.getLogger(__name__)


class ConfigCache:

    def __init__(self):
        self.memory = {}
        self.lock = Lock()

    def get(self, key):
        return self.memory[key]

    def update(self, key, value):
        logger.info(f"Update config key: {key} , value: {value}")
        with self.lock:
            self.memory[key] = value

        logger.info("Update config done.")
