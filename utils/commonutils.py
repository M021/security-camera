import yaml
import logging.config
from datetime import datetime
import os


def load_logging(log_file_path: str):
    with open(log_file_path, 'r') as stream:
        config = yaml.load(stream, Loader=yaml.FullLoader)
    logging.config.dictConfig(config)


def replace_datetime_with_current_time(orig_txt: str, date_format: str) -> str:
    return orig_txt.replace("<datetime>", datetime.now().strftime(date_format))


def make_folder_with_datetime(parent_dir: str, date_format: str, mode: int) -> str:
    now = datetime.now().strftime(date_format)
    output_dir_new = parent_dir + now + os.sep
    os.makedirs(output_dir_new, mode=mode)
    return output_dir_new
