# Security Camera with Raspberry Pi and Pi Camera

This project applies [TensorFlow Lite](https://tensorflow.org/lite) to perform
real-time detection using images streamed from the pi camera. Any person is detected,
it will trigger a series of actions such as image capture, email notification,
video recordings and recording backup to Google Drive. In addition, telegram bot acts
as a remote control to control the security camera so the owner can send instructions any time
to the raspberry pi server via Telegram. The reason to use Telegram bot as a remote control
is that it is a simple way without any costs. The user does not need to set up a server
to receive the instruction. Also, as long as your pi can connect to the outbound network
via HTTPS, you do not need to concern any networking issues such as firewall. However,
it is not highly recommended in terms of security and reliability.

## Tested environment and hardware

Pi Model: Raspberry Pi 4 Model B with 4 GB

Pi Camera: OV5647 Camera Module with built-in motorised IR-CUT fillets and two infrared LEDs.

OS: Raspbian GNU/Linux 11 (bullseye)

## Diagram
![Alt text](Diagram.png "Diagram")

## Set up
1. Retrieve security_cam project
```
curl https://gitlab.com/M021/security-camera.git
```

2. Retrieve my google_cloud project 
```
curl https://gitlab.com/M021/google-cloud.git
```

3. Run the program in a virtual environment. You may visit [this](https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/),
if you do not understand what it is.
```
 python3 -m venv <virtual-environment-name>

 source <virtual-environment-name>/bin/activate
```

4. Install google_cloud in your virtual environment.
```
 pip install --upgrade pip
 pip install <location_of_google_cloud_project>
```

5. Set up security camera project.
```
 sh <location_of_security_cam_project>/setup.sh
```

6. Configure the setting.ini in config/setting.in


7. Run below command to start up the application
```
python main.py --config config/setting.ini
```

8. [Optional] If you want to enable Google Drive service in the application, you may visit [this](https://learndataanalysis.org/google-drive-api-in-python-getting-started-lesson-1/)
   for the setup of client credential for this application


9. [Optional] If this is the first to connect your Google Drive using this application, you need to complete OAuth2.0 authentication process to grant the access right to this application.

Method 1: If you are using GUI, it will automatically pop up a browser connecting to google for authentication.
After you successfully authenticate, it will redirect to a localhost URL to pass the token to our application.

Method 2: If you are using CLI, you need to do authentication in another computer device which can use browser.
After you successfully authenticate, it will redirect to a localhost URL. By copying the URL and run below command
to manually call the URL in Raspberry Pi, the application can get the token.

```
curl "<localhost_url>"
```



## Useful link
If you are new to Raspberry Pi, you may visit [this](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up) for setup

If your Raspberry Pi is headless and want to access it via VNC, you may visit [this](
https://www.raspberrypi.com/documentation/computers/remote-access.html#creating-a-virtual-desktop)

If your Raspberry Pi can only connect to wireless enterprise environment, you may visit
[this](https://netbeez.net/blog/connect-your-raspberry-pi-to-wireless-enterprise-environments-with-wpa-supplicant/) and [this](https://unix.stackexchange.com/questions/333670/how-to-view-the-wpa2-peap-certificate-offered-by-an-ap)

If you want to understand more about object detection via TensorFlow, you may visit [this](https://www.youtube.com/watch?v=mNjXEybFn98) and [this](https://www.youtube.com/watch?v=-ZyFYniGUsw)

If you want to send out email notification via Google email server, you may visit [this](https://support.google.com/mail/thread/10950702/how-do-i-configure-gmail-to-send-email-from-my-software-specifically-the-apc-software?hl=en)
